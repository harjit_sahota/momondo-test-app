package com.jit_solutions.momondotestapp.core.integration.task.async.listener;

import com.jit_solutions.momondotestapp.core.AbstractViewModel;
import com.jit_solutions.momondotestapp.core.integration.task.error.web.IWebError;

public interface IWebResponseListener<T> {

    void success(T result);

    void error(IWebError error, T result);

    void done();

    void cancel();

    void preExecute();

    AbstractViewModel getContext();
}