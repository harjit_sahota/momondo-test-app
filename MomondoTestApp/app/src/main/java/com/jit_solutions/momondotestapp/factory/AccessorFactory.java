package com.jit_solutions.momondotestapp.factory;

import com.jit_solutions.momondotestapp.model.accessor.FlightSearchAccessor;

class AccessorFactory {
    static FlightSearchAccessor getFlightSearchAccessor() {
        return new FlightSearchAccessor(WebserviceFactory.getFlightService());
    }
}
