package com.jit_solutions.momondotestapp.model.provider.model;

import com.jit_solutions.momondotestapp.model.provider.dto.AirlineDTO;
import com.jit_solutions.momondotestapp.model.provider.dto.AirportDTO;
import com.jit_solutions.momondotestapp.model.provider.dto.FlightDTO;
import com.jit_solutions.momondotestapp.model.provider.dto.LegDTO;
import com.jit_solutions.momondotestapp.model.provider.dto.OfferDTO;
import com.jit_solutions.momondotestapp.model.provider.dto.SegmentDTO;
import com.jit_solutions.momondotestapp.model.provider.dto.SupplierDTO;
import com.jit_solutions.momondotestapp.model.provider.dto.TicketClassDTO;

import java.io.Serializable;
import java.util.List;

public class FlightSearchModel implements Serializable {
    private String originName, originCode, destinationName, destinationCode;
    private List<AirlineDTO> airlines;
    private List<AirportDTO> airports;
    private List<TicketClassDTO> ticketClasses;
    private List<FlightDTO> flights;
    private List<LegDTO> legs;
    private List<SegmentDTO> segments;
    private List<SupplierDTO>  suppliers;
    private List<OfferDTO> offers;

    public FlightSearchModel(String originName, String originCode, String destinationName, String destinationCode, List<AirlineDTO> airlines, List<AirportDTO> airports, List<TicketClassDTO> ticketClasses, List<FlightDTO> flights, List<LegDTO> legs, List<SegmentDTO> segments, List<SupplierDTO> suppliers, List<OfferDTO> offers) {
        this.originName = originName;
        this.originCode = originCode;
        this.destinationName = destinationName;
        this.destinationCode = destinationCode;
        this.airlines = airlines;
        this.airports = airports;
        this.ticketClasses = ticketClasses;
        this.flights = flights;
        this.legs = legs;
        this.segments = segments;
        this.suppliers = suppliers;
        this.offers = offers;
    }

    public String getOriginName() {
        return originName;
    }

    public String getOriginCode() {
        return originCode;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public List<AirlineDTO> getAirlines() {
        return airlines;
    }

    public List<AirportDTO> getAirports() {
        return airports;
    }

    public List<TicketClassDTO> getTicketClasses() {
        return ticketClasses;
    }

    public List<FlightDTO> getFlights() {
        return flights;
    }

    public List<LegDTO> getLegs() {
        return legs;
    }

    public List<SegmentDTO> getSegments() {
        return segments;
    }

    public List<SupplierDTO> getSuppliers() {
        return suppliers;
    }

    public List<OfferDTO> getOffers() {
        return offers;
    }
}
