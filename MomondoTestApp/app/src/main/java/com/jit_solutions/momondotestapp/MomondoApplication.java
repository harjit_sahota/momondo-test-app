package com.jit_solutions.momondotestapp;

import android.app.Application;

import com.jit_solutions.momondotestapp.core.configuration.SystemConfiguration;


public class MomondoApplication extends Application {
    private static MomondoApplication mApplicationContext = null;
    private static SystemConfiguration mSystemConfiguration;

    /**
     * Get the getContext of the application
     */
    public static MomondoApplication getOkapiApplicationContext() {
        return mApplicationContext;
    }

    /**
     * Get the systemconfiguration of the application
     */
    public static SystemConfiguration getSystemConfiguration() {
        return mSystemConfiguration;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationContext = this;
        mSystemConfiguration = new SystemConfiguration();
    }
}
