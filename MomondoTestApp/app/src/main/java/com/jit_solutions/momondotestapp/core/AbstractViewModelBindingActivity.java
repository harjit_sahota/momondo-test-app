package com.jit_solutions.momondotestapp.core;

import android.content.Context;
import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public abstract class AbstractViewModelBindingActivity<Binding extends ViewDataBinding, ViewModel extends AbstractViewModel> extends AppCompatActivity implements IViewInterface, ViewModelBindingHelper.Callback<ViewModel>, AbstractViewModelBindingFragment.Callback {

    private ViewModelBindingHelper<Binding, ViewModel> mViewModelBindingHelper = new ViewModelBindingHelper<>();;

    protected abstract ViewModel setViewModel();

    protected abstract int getResourceId();

    private boolean mBackPressedEnabled = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModelBindingHelper.onCreate(this, getResourceId(), savedInstanceState, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        Config.collectLifecycleData(this);
        mViewModelBindingHelper.onResume();
    }

    @Override
    protected void onPause() {
//        Config.pauseCollectingLifecycleData();
        mViewModelBindingHelper.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mViewModelBindingHelper.onDestroy(this);
        super.onDestroy();
    }

    @CallSuper
    @Override
    public void onBackPressed() {
        if (mBackPressedEnabled) {
            onBackPressedEnabled();
        }
    }

    protected void onBackPressedEnabled() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mViewModelBindingHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mViewModelBindingHelper.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }


    protected ViewModel getViewModel() {
        return mViewModelBindingHelper.getViewModel();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public Binding getBinding() {
        return mViewModelBindingHelper.getBinding();
    }

    @Override
    public void enableProgressIndicator(boolean enableProgressIndicator) {
        if (enableProgressIndicator) {
            mViewModelBindingHelper.showProgressIndicator();
        } else {
            mViewModelBindingHelper.hideProgressIndicator();
        }
    }

    @Override
    public void setBackPressedEnabled(boolean enabled) {
        mBackPressedEnabled = enabled;
    }

    @Override
    public void onViewModelInitialized(ViewModel viewModel) {
        //Do nothing in base
    }

    @Override
    public ViewModel createViewModel() {
        return setViewModel();
    }

    protected boolean enableScreenshot() {
        return true;
    }
}
