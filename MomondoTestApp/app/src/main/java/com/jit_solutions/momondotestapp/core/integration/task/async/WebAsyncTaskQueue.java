package com.jit_solutions.momondotestapp.core.integration.task.async;

import java.util.concurrent.Executor;

public class WebAsyncTaskQueue {

    public interface Task {
        Object execute();

        Object executeOnExecutor(Executor executor);
    }

    private static WebAsyncTaskQueue mQueue = new WebAsyncTaskQueue();

    public static WebAsyncTaskQueue getInstance() {
        return mQueue;
    }

    public synchronized void add(Task task) {
        add(task, null);
    }

    public synchronized void add(Task task, Executor executor) {
        if (executor != null) {
            task.executeOnExecutor(executor);
        } else {
            task.execute();
        }

    }
}
