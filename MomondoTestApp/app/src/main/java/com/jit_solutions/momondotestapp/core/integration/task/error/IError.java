package com.jit_solutions.momondotestapp.core.integration.task.error;

import java.io.Serializable;

public interface IError extends Serializable {

    String getMessage();

    String getTitle();
}
