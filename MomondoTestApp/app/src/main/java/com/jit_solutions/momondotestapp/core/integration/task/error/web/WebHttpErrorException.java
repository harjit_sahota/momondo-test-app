package com.jit_solutions.momondotestapp.core.integration.task.error.web;

import com.jit_solutions.momondotestapp.MomondoApplication;
import com.jit_solutions.momondotestapp.R;

public class WebHttpErrorException extends RuntimeException implements IWebError {

    private final String mMessage;
    private final int mStatusCode;

    public WebHttpErrorException(String message, int statusCode) {
        super();
        mMessage = message;
        mStatusCode = statusCode;
    }

    @Override
    public String getTitle() {
        return MomondoApplication.getOkapiApplicationContext().getString(R.string.error_generic_title);
    }

    @Override
    public String getMessage() {
        return mMessage;
    }

    @Override
    public WebErrorCodeEnum getErrorCode() {
        if (mStatusCode == -1) return null;

        for (WebErrorCodeEnum webErrorCodeEnum : WebErrorCodeEnum.values()) {
            if (webErrorCodeEnum.getStatusCode() == mStatusCode) {
                return webErrorCodeEnum;
            }
        }
        return null;
    }

}
