package com.jit_solutions.momondotestapp.core;

import android.content.Context;
import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class AbstractViewModelBindingFragment<Binding extends ViewDataBinding, ViewModel extends AbstractViewModel> extends Fragment implements IViewInterface, ViewModelBindingHelper.Callback<ViewModel> {

    private final ViewModelBindingHelper<Binding, ViewModel> mViewModelBindingHelper = new ViewModelBindingHelper<>();
    private Callback mCallback;

    protected abstract ViewModel setViewModel();

    protected abstract int getResourceId();


    public interface Callback {
        void enableProgressIndicator(boolean enableProgressIndicator);

        void setBackPressedEnabled(boolean enabled);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (Callback) context;
    }

    @Deprecated
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mViewModelBindingHelper.onCreate(this, getResourceId(), savedInstanceState, this);
        return mViewModelBindingHelper.getBinding().getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModelBindingHelper.onActivityCreated();
    }


    @Override
    public void onResume() {
        super.onResume();
        mViewModelBindingHelper.onResume();
    }

    @Override
    public void onPause() {
        mViewModelBindingHelper.onPause();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        mViewModelBindingHelper.onDestroyView(this);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        mViewModelBindingHelper.onDestroy(this);
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mViewModelBindingHelper.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mViewModelBindingHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(mViewModelBindingHelper == null) return;
        if (isVisibleToUser) {
            mViewModelBindingHelper.onFragmentVisibleToUser();
        } else {
            mViewModelBindingHelper.onFragmentNotVisibleToUser();
        }
    }

    protected ViewModel getViewModel() {
        return mViewModelBindingHelper.getViewModel();
    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    @Override
    public Binding getBinding() {
        return mViewModelBindingHelper.getBinding();
    }

    @Override
    public void enableProgressIndicator(boolean enableProgressIndicator) {
        mCallback.enableProgressIndicator(enableProgressIndicator);
    }

    @Override
    public void setBackPressedEnabled(boolean enabled) {
        mCallback.setBackPressedEnabled(enabled);
    }

    @Override
    public void onViewModelInitialized(ViewModel viewModel) {
        //Do nothing in base
    }

    @Override
    public ViewModel createViewModel() {
        return setViewModel();
    }
}
