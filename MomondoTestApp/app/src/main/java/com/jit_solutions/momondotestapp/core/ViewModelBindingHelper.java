package com.jit_solutions.momondotestapp.core;


import android.app.Activity;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.jit_solutions.momondotestapp.BR;
import com.jit_solutions.momondotestapp.R;
import com.jit_solutions.momondotestapp.databinding.BaseActivityBinding;

import java.util.UUID;

public class ViewModelBindingHelper<Binding extends ViewDataBinding, ViewModel extends AbstractViewModel> extends BaseObservable {

    public interface Callback<ViewModel> {
        void onViewModelInitialized(ViewModel viewModel);

        ViewModel createViewModel();
    }

    private static final String TAG_BINDING_HELPER = ".TAG_BINDING_HELPER";
    private static final String TAG_VIEW_MODEL_ID = ".TAG_VIEW_MODEL_ID";
    private static final String DATA_VIEWMODEL_CLASS_NAME = ViewModelBindingHelper.class.getSimpleName() + ".DATA_VIEWMODEL_CLASS_NAME";
    private String mViewModelId;
    private boolean mAlreadyCreated;
    private boolean mModelRemoved;
    private ViewModel mViewModel;
    private Binding mBinding;
    private boolean mIsProgressIndicatorEnabled;
    private String mViewModelClassName;
    private IViewInterface mViewInterface;

    public ViewModel getViewModel() {
        return mViewModel;
    }

    public Binding getBinding() {
        return mBinding;
    }

    public void onCreate(IViewInterface viewInterface, @LayoutRes int resourceId, @Nullable Bundle savedInstanceState, Callback callback) {

        if (mAlreadyCreated) return;
        mAlreadyCreated = true;

        if (callback == null) {
            //If callback or callback are null
            return;
        }

        mViewInterface = viewInterface;
        if (viewInterface instanceof Activity) {
            BaseActivityBinding mBaseBinding = DataBindingUtil.setContentView(((Activity) viewInterface), R.layout.base_activity);
            //Set the helper for base activity
            mBaseBinding.setHelper(this);
            mBinding = DataBindingUtil.inflate(LayoutInflater.from(viewInterface.getContext()), resourceId, mBaseBinding.baseActivityContainer, true);
        } else if (viewInterface instanceof Fragment) {
            mBinding = DataBindingUtil.inflate(LayoutInflater.from(viewInterface.getContext()), resourceId, (ViewGroup) ((Fragment) viewInterface).getActivity().findViewById(R.id.baseActivityContainer), false);
        } else
            throw new IllegalArgumentException("View must be an instance of Activity or Fragment.");

        if (mViewModelId == null) {
            if (savedInstanceState == null)
                mViewModelId = UUID.randomUUID().toString();
            else {
                mViewModelClassName = savedInstanceState.getString(DATA_VIEWMODEL_CLASS_NAME);
                mViewModelId = savedInstanceState.getString(getViewModelIdFieldName());
            }
        }

        ViewModelProvider.ViewModelWrapper<ViewModel> viewModelWrapper = ViewModelProvider.getInstance().getViewModelWrapper(mViewModelId);

        if (viewModelWrapper == null) {
            viewModelWrapper = ViewModelProvider.getInstance().createViewModelWrapper(mViewModelId, (ViewModel) callback.createViewModel());
        }
        mViewModel = viewModelWrapper.getViewModel();
        mViewModelClassName = mViewModel.getClass().getName();
        mViewModel.setView(viewInterface);

        if (viewModelWrapper.wasCreated()) {

            if (callback != null) {
                callback.onViewModelInitialized(mViewModel);
            }
            mViewModel.onViewModelCreated();
        }

        mViewModel.onCreate(viewModelWrapper.wasCreated());
    }

    public void onResume() {
        if (mViewModel != null) {
            mViewModel.onResume();
            mViewModel.internalRunAllUiTasksInQueue();
        }
    }

    public void onPause() {
        if (mViewModel != null) mViewModel.onPause();
    }

    public void onActivityCreated() {
        if (mViewModel != null) {
            mViewModel.onActivityCreated();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mViewModel.onActivityResult(requestCode, resultCode, data);
    }

    void onSaveInstanceState(@NonNull Bundle bundle) {
        bundle.putString(DATA_VIEWMODEL_CLASS_NAME, mViewModelClassName);
        bundle.putString(getViewModelIdFieldName(), mViewModelId);
    }

    public void onDestroyView(@NonNull Fragment fragment) {
        if (mViewModel == null) return;

        if (fragment.getActivity() != null && fragment.getActivity().isFinishing()) {
            mViewModel.onViewDetached(true);
            removeViewModel();
        } else {
            mViewModel.onViewDetached(false);
            mAlreadyCreated = false;
        }
        mBinding = null;
    }


    public void onDestroy(@NonNull Activity activity) {
        if (mViewModel == null) return;

        if (activity.isFinishing()) {
            mViewModel.onViewDetached(true);
            removeViewModel();
        } else
            mViewModel.onViewDetached(false);
        mAlreadyCreated = false;
        mBinding = null;
    }

    public void onDestroy(@NonNull Fragment fragment) {
        if (mViewModel == null) return;

        if (fragment.getActivity().isFinishing()) {
            removeViewModel();
        } else if (fragment.isRemoving()) {
            Log.d(TAG_BINDING_HELPER, "Removing viewmodel - fragment replaced");
            removeViewModel();
        }
        mAlreadyCreated = false;
    }


    public void onFragmentVisibleToUser() {
        if (mViewModel == null) return;
        mViewModel.onFragmentVisibleToUser();
    }

    public void onFragmentNotVisibleToUser() {
        if (mViewModel == null) return;
        mViewModel.onFragmentNotVisibleToUser();
    }

    @NonNull
    private String getViewModelIdFieldName() {
        return TAG_VIEW_MODEL_ID + mViewInterface.getClass().getName() + " " + mViewModelClassName;
    }

    private void removeViewModel() {
        if (!mModelRemoved) {
            ViewModelProvider.getInstance().removeViewModel(mViewModelId);
            mViewModel.onViewModelDestroyed();
            mModelRemoved = true;
            mAlreadyCreated = false;
        }
    }
    @Bindable
    public boolean isProgressIndicatorEnabled() {
        return mIsProgressIndicatorEnabled;
    }

    protected void showProgressIndicator() {
        if (!mIsProgressIndicatorEnabled) {
            mIsProgressIndicatorEnabled = true;
            notifyPropertyChanged(BR.progressIndicatorEnabled);
        }
    }

    protected void hideProgressIndicator() {
        if (mIsProgressIndicatorEnabled) {
            mIsProgressIndicatorEnabled = false;
            notifyPropertyChanged(BR.progressIndicatorEnabled);
        }
    }
}
