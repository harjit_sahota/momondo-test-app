package com.jit_solutions.momondotestapp.flight_results;


import android.os.Bundle;
import android.support.annotation.Nullable;

import com.jit_solutions.momondotestapp.R;
import com.jit_solutions.momondotestapp.core.AbstractViewModelBindingFragment;
import com.jit_solutions.momondotestapp.databinding.FlightFragmentBinding;
import com.jit_solutions.momondotestapp.factory.ViewModelFactory;
import com.jit_solutions.momondotestapp.model.provider.model.FlightSearchModel;

public class FlightFragment extends AbstractViewModelBindingFragment<FlightFragmentBinding, FlightViewModel> {
    private static final String DATA_FLIGHT_SEARCH_MODEL = FlightFragment.class.getSimpleName() + " DATA_FLIGHT_SEARCH_MODEL";

    public static FlightFragment newInstance(FlightSearchModel model) {
        FlightFragment fragment = new FlightFragment();
        fragment.setArguments(new Bundle());
        fragment.getArguments().putSerializable(DATA_FLIGHT_SEARCH_MODEL, model);
        return fragment;
    }

    public FlightFragment() {

    }

    @Override
    protected FlightViewModel setViewModel() {
        return ViewModelFactory.getFlightViewModel();
    }

    @Override
    protected int getResourceId() {
        return R.layout.flight_fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getViewModel().getFlightSearchModel((FlightSearchModel) getArguments().getSerializable(DATA_FLIGHT_SEARCH_MODEL));
    }
}
