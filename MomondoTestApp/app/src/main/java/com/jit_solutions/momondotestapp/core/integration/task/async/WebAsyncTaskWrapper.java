package com.jit_solutions.momondotestapp.core.integration.task.async;

import android.os.AsyncTask;

import java.util.concurrent.Executor;

public abstract class WebAsyncTaskWrapper<Params, Result> extends AsyncTask<Params, Void, Result> implements WebAsyncTaskQueue.Task {

    private Params[] mParams;

    public WebAsyncTaskWrapper(Params... params) {
        this.mParams = params;
    }

    public abstract boolean doCancel(boolean mayInterruptIfRunning);

    @Override
    public AsyncTask<Params, Void, Result> execute() {
        return this.execute(mParams);
    }

    @Override
    public Object executeOnExecutor(Executor executor) {
        return this.executeOnExecutor(executor, mParams);
    }
}