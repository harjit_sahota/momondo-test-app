package com.jit_solutions.momondotestapp.core.integration.task.error.web;

import com.jit_solutions.momondotestapp.MomondoApplication;
import com.jit_solutions.momondotestapp.R;

public class WebTimeoutException extends RuntimeException implements IWebError {

    @Override
    public String getTitle() {
        return MomondoApplication.getOkapiApplicationContext().getString(R.string.error_generic_title);
    }

    @Override
    public String getMessage() {
        return MomondoApplication.getOkapiApplicationContext().getString(R.string.error_generic_label);
    }

    @Override
    public WebErrorCodeEnum getErrorCode() {
        return WebErrorCodeEnum.CLIENT_TIMEOUT;
    }
}
