package com.jit_solutions.momondotestapp.core.integration.task.async.listener;


import com.jit_solutions.momondotestapp.core.AbstractViewModel;

public interface IContextProviderListener {
    AbstractViewModel getContextProviderForAsyncTask();
}
