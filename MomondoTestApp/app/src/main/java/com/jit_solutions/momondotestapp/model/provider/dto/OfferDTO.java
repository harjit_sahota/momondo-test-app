package com.jit_solutions.momondotestapp.model.provider.dto;

public class OfferDTO {
    private int id, flightIndex, ticketClassIndex;
    private double price;
}
