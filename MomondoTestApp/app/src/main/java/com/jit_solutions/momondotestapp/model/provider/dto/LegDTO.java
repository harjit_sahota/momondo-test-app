package com.jit_solutions.momondotestapp.model.provider.dto;

public class LegDTO {
    private int id, airlineIndex, flightNumber, destinationIndex, originIndex;
    private long departure, arrival;

    public LegDTO(int id, int airlineIndex, int flightNumber, int destinationIndex, int originIndex, long departure, long arrival) {
        this.id = id;
        this.airlineIndex = airlineIndex;
        this.flightNumber = flightNumber;
        this.destinationIndex = destinationIndex;
        this.originIndex = originIndex;
        this.departure = departure;
        this.arrival = arrival;
    }

    public int getId() {
        return id;
    }

    public int getAirlineIndex() {
        return airlineIndex;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public int getDestinationIndex() {
        return destinationIndex;
    }

    public int getOriginIndex() {
        return originIndex;
    }

    public long getDeparture() {
        return departure;
    }

    public long getArrival() {
        return arrival;
    }
}
