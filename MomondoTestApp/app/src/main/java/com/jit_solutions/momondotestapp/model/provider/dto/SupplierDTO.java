package com.jit_solutions.momondotestapp.model.provider.dto;

import java.util.List;

public class SupplierDTO {
    private int id;
    private String name;
    private List<Integer> offerIndices;
}
