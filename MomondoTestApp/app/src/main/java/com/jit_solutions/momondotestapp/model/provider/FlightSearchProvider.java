package com.jit_solutions.momondotestapp.model.provider;

import com.jit_solutions.momondotestapp.core.integration.task.async.WebAbstractAsyncTask;
import com.jit_solutions.momondotestapp.core.integration.task.async.listener.IWebResponseListener;
import com.jit_solutions.momondotestapp.model.provider.model.FlightSearchModel;

import java.util.LinkedHashMap;

public class FlightSearchProvider {
    private IFlightSearchAccessor mAccessor;

    public FlightSearchProvider(IFlightSearchAccessor mAccessor) {
        this.mAccessor = mAccessor;
    }

    public void getFlightSearchResults(final LinkedHashMap<String, Object> bodyParams, IWebResponseListener<FlightSearchModel> listener) {
        (new WebAbstractAsyncTask<Void, FlightSearchModel>(listener) {

            @Override
            protected FlightSearchModel doInBackgroundImp(Void... params) throws Exception {
                return mAccessor.getFlightSearch(bodyParams);
            }

        }).execute();
    }

}
