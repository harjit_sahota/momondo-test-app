package com.jit_solutions.momondotestapp.model.provider.dto;

import java.util.List;

public class SegmentDTO {
    private int id, duration;
    private List<Integer> legIndices;
}
