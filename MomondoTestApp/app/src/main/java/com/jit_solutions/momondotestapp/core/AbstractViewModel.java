package com.jit_solutions.momondotestapp.core;


import android.content.Intent;
import android.content.res.Resources;
import android.databinding.BaseObservable;
import android.databinding.ViewDataBinding;
import android.os.Handler;
import android.support.annotation.CallSuper;
import android.view.View;

import com.jit_solutions.momondotestapp.R;
import com.jit_solutions.momondotestapp.core.integration.task.async.listener.IContextProviderListener;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class AbstractViewModel<Binding extends ViewDataBinding> extends
        BaseObservable implements IContextProviderListener {

    private IViewInterface<Binding> mViewInterface;
    private boolean mRunning;

    public Handler getHandler() {
        return mHandler;
    }

    private Handler mHandler = new Handler();
    private Thread mUiThread;
    private Queue<Runnable> mUiThreadTaskQueue;
    private boolean mProgressIndicatorRunning;
    private boolean mBackPressedEnabled = true;

    public AbstractViewModel() {
        mUiThreadTaskQueue = new ConcurrentLinkedQueue<>();
    }

    protected abstract void addViewModelObservableBinding(Binding binding);

    protected IViewInterface<Binding> getViewInterface() {
        return mViewInterface;
    }

    @Override
    public AbstractViewModel getContextProviderForAsyncTask() {
        return this;
    }

    public void setView(IViewInterface<Binding> viewInterface) {
        mViewInterface = viewInterface;
        addViewModelObservableBinding(viewInterface.getBinding());
    }

    protected Binding getBinding() {
        if (mViewInterface == null) {
            return null;
        }
        return mViewInterface.getBinding();
    }

    /**
     * onResume callback from the View
     */


    public void onResume() {
        mRunning = true;
    }

    /**
     * onPause callback from the View
     */
    @CallSuper
    public void onPause() {
        mRunning = false;
    }

    /**
     * Called after the ViewModel is instantiated
     */
    @CallSuper
    public void onViewModelCreated() {
        mUiThread = Thread.currentThread();
    }

    /**
     * Called after the View is attached to this ViewModel
     *
     * @param firstAttachment flag indicating whether the ViewModel was just created and attached for the first time
     */
    @CallSuper
    protected void onCreate(boolean firstAttachment) {
    }

    /**
     * Called after the View is being destroyed and therefore detached from the ViewModel
     *
     * @param finalDetachment flag indicating whether it is final detachment and this instance of ViewModel will no longer be used
     */
    @CallSuper
    protected void onViewDetached(boolean finalDetachment) {
        mViewInterface = null;
    }

    /**
     * Called after this ViewModel instance was destroyed and removed from cache
     * <p>
     * This is a place to do any cleanup to avoid memory leaks
     */
    @CallSuper
    protected void onViewModelDestroyed() {
        mViewInterface = null;
        mUiThreadTaskQueue.clear();
    }

    /**
     * When viewpager fragment view is visible to the user.
     *
     * @return true if View is currently attached
     */
    @CallSuper
    public void onFragmentVisibleToUser() {

    }

    /**
     * When viewpager fragment view is not visible to the user.
     *
     * @return true if View is currently attached
     */
    @CallSuper
    public void onFragmentNotVisibleToUser() {
    }

    /**
     * When activity is created - only fragments use this
     */
    @CallSuper
    public void onActivityCreated() {

    }


    /**
     * Helper method to determine if View is attached at the moment
     *
     * @return true if View is currently attached
     */
    public boolean hasViewAttached() {
        return mViewInterface != null && mViewInterface.getBinding() != null;
    }

    /**
     * Returns true if the Activity/Fragment is in running state(not paused) at the moment
     *
     * @return true if running
     */
    public boolean isRunning() {
        return hasViewAttached() && mRunning;
    }

    /**
     * onActivityResult callback from the View
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    public void startActivity(Intent intent) {
        getViewInterface().getContext().startActivity(intent);
    }


    /**
     * Convenience method to retrieve Resources from Context resources
     * <p>
     * Warning: May return null if View is not attached
     *
     * @return Resources or null
     */
    protected Resources getResources() {
        if (getViewInterface().getContext() == null)
            return null;
        return getViewInterface().getContext().getResources();
    }

    protected void showProgressIndicator() {
        if (getViewInterface().getBinding() == null) return;
        View view = getViewInterface().getBinding().getRoot().findViewById(R.id.spinner);

        if (view != null) {
            view.setVisibility(View.VISIBLE);

        } else {
            getViewInterface().enableProgressIndicator(true);
        }
        progressIndicatorRunning();
    }

    protected void hideProgressIndicator() {
        if (getViewInterface() == null) return;
        View view = getViewInterface().getBinding().getRoot().findViewById(R.id.spinner);

        if (view != null) {
            view.setVisibility(View.GONE);

        } else {
            getViewInterface().enableProgressIndicator(false);
        }
        progressIndicatorStopped();
    }


    protected boolean isProgressIndicatorRunning() {
        return mProgressIndicatorRunning;
    }


    protected void progressIndicatorRunning() {
        mProgressIndicatorRunning = true;
    }


    protected void progressIndicatorStopped() {
        mProgressIndicatorRunning = false;
    }

    /**
     * Convenience method for Handler.postDelayed()
     *
     * @param runnable Runnable to run
     * @param delayMs  Delay in ms
     */
    public void postDelayed(Runnable runnable, long delayMs) {
        mHandler.postDelayed(runnable, delayMs);
    }

    /**
     * Runs the specified action on the UI thread. If the current thread is the UI
     * thread, then the action is executed immediately. If the current thread is
     * not the UI thread, the action is posted to the event queue of the UI thread.
     *
     * @param action the action to run on the UI thread
     */
    public final void runOnUiThread(Runnable action) {
        internalRunOnUiThreadNow(action);
    }

    void internalRunAllUiTasksInQueue() {
        while (!mUiThreadTaskQueue.isEmpty()) {
            internalRunOnUiThreadNow(mUiThreadTaskQueue.poll());
        }
    }

    private void internalRunOnUiThreadNow(final Runnable action) {
        if (Thread.currentThread() != mUiThread) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (hasViewAttached())
                                action.run();
                            else
                                mUiThreadTaskQueue.add(action);
                        }
                    });
                }
            }).start();
        } else {
            if (hasViewAttached())
                action.run();
            else
                mUiThreadTaskQueue.add(action);
        }
    }

    protected String getString(int resourceId) {
        return getViewInterface().getContext().getString(resourceId);
    }

    protected void setBackPressedEnabled(boolean enabled) {
        getViewInterface().setBackPressedEnabled(enabled);
    }


}
