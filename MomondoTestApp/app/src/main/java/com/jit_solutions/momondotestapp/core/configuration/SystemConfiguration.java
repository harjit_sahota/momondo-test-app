package com.jit_solutions.momondotestapp.core.configuration;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.jit_solutions.momondotestapp.MomondoApplication;

public class SystemConfiguration {

    public SystemConfiguration() {
    }

    public boolean hasInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) MomondoApplication.getOkapiApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return !(netInfo == null || !netInfo.isConnectedOrConnecting());
    }
}
