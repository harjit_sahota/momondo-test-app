package com.jit_solutions.momondotestapp.factory;

import com.jit_solutions.momondotestapp.MainViewModel;
import com.jit_solutions.momondotestapp.flight_results.FlightViewModel;

public class ViewModelFactory {

    public static MainViewModel getMainViewModel() {
        return new MainViewModel(ProviderFactory.getFlightSearchProvider());
    }

    public static FlightViewModel getFlightViewModel() {
        return new FlightViewModel();
    }
}
