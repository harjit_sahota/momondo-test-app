package com.jit_solutions.momondotestapp;

import android.app.DatePickerDialog;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import com.jit_solutions.momondotestapp.core.AbstractViewModel;
import com.jit_solutions.momondotestapp.core.integration.task.async.listener.AbstractWebProviderListener;
import com.jit_solutions.momondotestapp.databinding.ActivityMainBinding;
import com.jit_solutions.momondotestapp.model.provider.FlightSearchProvider;
import com.jit_solutions.momondotestapp.model.provider.model.FlightSearchModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

public class MainViewModel extends AbstractViewModel<ActivityMainBinding> {

    interface Callback {
        void flightSearchResults(FlightSearchModel model);
    }

    private Callback mCallback;
    private FlightSearchProvider mProvider;
    private int mYear;
    private int mMonth;
    private int mDay;

    private long mDepartureDate, mReturnDate;

    public MainViewModel(FlightSearchProvider mProvider) {
        this.mProvider = mProvider;
    }

    @Override
    protected void addViewModelObservableBinding(ActivityMainBinding binding) {
        binding.setViewmodel(this);
    }

    @Override
    public void onViewModelCreated() {
        super.onViewModelCreated();
        mCallback = (Callback) getViewInterface().getContext();
    }

    private void getCurrentDate() {
        Calendar mCalendar = Calendar.getInstance();
        mYear = mCalendar.get(Calendar.YEAR);
        mMonth = mCalendar.get(Calendar.MONTH);
        mDay = mCalendar.get(Calendar.DAY_OF_MONTH);
    }

    void searchForFlights() {
        LinkedHashMap<String, Object> mBodyParams = new LinkedHashMap<>();
        mBodyParams.put("origin", getBinding().fromEdittext.getText().toString());
        mBodyParams.put("destination", getBinding().toEdittext.getText().toString());
        mBodyParams.put("departureDate", mDepartureDate);
        mBodyParams.put("returnDate", mReturnDate);
        mBodyParams.put("ticketCount", 1);

        mProvider.getFlightSearchResults(mBodyParams, new AbstractWebProviderListener<FlightSearchModel>(this) {

            @Override
            public void preExecute() {
                super.preExecute();
                showProgressIndicator();
            }

            @Override
            public void success(FlightSearchModel model) {
                super.success(model);
                mCallback.flightSearchResults(model);
            }

            @Override
            public void done() {
                super.done();
                hideProgressIndicator();
            }
        });
    }

    void getDate(final View v) {
        getCurrentDate();
        DatePickerDialog datePickerDialog = new DatePickerDialog(getViewInterface().getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int month = monthOfYear + 1;

                ((Button) v).setText(dayOfMonth + "/" + month + "/" + year);

                DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                try {
                    Date date = formatter.parse(dayOfMonth + "-" + month + "-" + year);
                    if (v.getId() == R.id.start_date_btn)
                        mDepartureDate = date.getTime();

                    else
                        mReturnDate = date.getTime();

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (isProgressIndicatorRunning())
            hideProgressIndicator();
    }
}
