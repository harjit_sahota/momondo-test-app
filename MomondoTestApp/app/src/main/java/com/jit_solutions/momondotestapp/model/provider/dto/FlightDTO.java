package com.jit_solutions.momondotestapp.model.provider.dto;

import java.util.List;

public class FlightDTO {
    public int id;
    public int ticketClassIndex;
    public List<Integer> segmentIndices;

}
