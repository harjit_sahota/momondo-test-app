package com.jit_solutions.momondotestapp.core.utils;

import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public class GenericListener {

    private static Map<Class<?>, Set<Object>> listeners = new WeakHashMap<>();
    private static final String TAG = GenericListener.class.getSimpleName();

    public interface Visitor<T> {
        void visit(T listener);
    }

    public static <T> void registerListener(T listener, Class<T> clazz) {
        if (listener == null) {
            Log.w(TAG, "trying to register listener which is null");
            return;
        }

        if (!listeners.containsKey(clazz)) {
            listeners.put(clazz, new HashSet<>(2));
        }
        boolean added = listeners.get(clazz).add(listener);
        if (!added) {
            Log.w(TAG, "listener[" + listener + "] already added as listener");
        }
    }


    public static <T> void unregisterListener(T listener, Class<T> clazz) {
        if (listeners.containsKey(clazz)) {
            listeners.get(clazz).remove(listener);
        }
    }

    public static void unregisterListeners() {
        listeners.clear();
    }


    public static <T> void notify(final Visitor<T> v, final Class<T> clazz) {
        if (listeners.get(clazz) == null) return;

        final List<Object> listenersList = new ArrayList<>(listeners.get(clazz));

        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    notifyListeners(v, listenersList);
                    return null;
                }
            }.execute();
        } else {
            notifyListeners(v, listenersList);
        }
    }

    @SuppressWarnings("unchecked")
    private static <T> void notifyListeners(Visitor<T> v, List<Object> listenersList) {
        if (listenersList != null) {
            for (Object o : listenersList) {
                v.visit((T) o);
            }
        }
    }
}
