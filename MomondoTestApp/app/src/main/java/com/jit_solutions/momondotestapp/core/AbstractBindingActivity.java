package com.jit_solutions.momondotestapp.core;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public abstract class AbstractBindingActivity<Binding extends ViewDataBinding> extends AppCompatActivity {

    private Binding mBinding;

    protected Binding getBinding() {
        return mBinding;
    }

    protected abstract int getResourceId();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, getResourceId());
    }

    @Override
    protected void onResume() {
        super.onResume();
//        Config.collectLifecycleData(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        Config.pauseCollectingLifecycleData();
    }

}
