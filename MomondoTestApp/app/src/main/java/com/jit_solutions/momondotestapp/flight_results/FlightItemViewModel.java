package com.jit_solutions.momondotestapp.flight_results;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.jit_solutions.momondotestapp.model.provider.model.FlightResultModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FlightItemViewModel extends BaseObservable {
    private FlightResultModel mModel;
    private String mFormat;

    FlightItemViewModel(FlightResultModel mModel) {
        this.mModel = mModel;
        mFormat = "dd MMMM, yyyy hh:mma";
    }

    @Bindable
    public String getFlightNumber() {
        return "Flight nr: " + mModel.getFlightNumber();
    }

    @Bindable
    public String getOrigin() {
        return mModel.getOrigin();
    }

    @Bindable
    public String getDestination() {
        return mModel.getDestination();
    }

    @Bindable
    public String getAirline() {
        return mModel.getAirline();
    }

    @Bindable
    public String getDepartureTime() {
        Date df = new Date(mModel.getDeparture());
        return new SimpleDateFormat(mFormat, Locale.getDefault()).format(df);
    }

    @Bindable
    public String getArrivalTime() {
        Date df = new Date(mModel.getArrival());
        return new SimpleDateFormat(mFormat, Locale.getDefault()).format(df);
    }
}
