package com.jit_solutions.momondotestapp.core;

import android.support.annotation.NonNull;

import java.util.HashMap;


/**
 * Static Singleton class providing ViewModel instances based on their unique identifier.
 * <p>
 * The instance is either created using default constructor or if it exists - retrieved from a static in-memory
 * map storing previously created instances.
 */
public class ViewModelProvider {

    private static ViewModelProvider mInstance;

    /**
     * HashMap storing ViewModel instances
     */
    private final HashMap<String, AbstractViewModel> mViewModels;


    private ViewModelProvider() {
        mViewModels = new HashMap<>();
    }


    /**
     * Static instance getter
     *
     * @return static {@link ViewModelProvider} instance
     */
    public static ViewModelProvider getInstance() {
        if (mInstance == null)
            mInstance = new ViewModelProvider();
        return mInstance;
    }


    /**
     * Remove a specific ViewModel from static HashMap.
     * Call this as soon as you are sure the ViewModel won't be used anymore
     *
     * @param viewModelId Unique ViewModel ID used to store the ViewModel instance
     */
    public synchronized void removeViewModel(String viewModelId) {
        mViewModels.remove(viewModelId);
    }

    public synchronized <T extends AbstractViewModel> ViewModelWrapper<T> getViewModelWrapper(String viewModelId) {
        //Get instance from memory
        ViewModelWrapper wrapper = null;
        T instance = (T) mViewModels.get(viewModelId);
        if (instance != null) {
            wrapper = new ViewModelWrapper<T>(instance, false);
        }
        return wrapper;
    }

    @NonNull
    public synchronized <T extends AbstractViewModel> ViewModelWrapper<T> createViewModelWrapper(String viewModelId, T viewModel) {
        // If not exist
        try {
            if (viewModel == null) {
                throw new IllegalArgumentException("createViewModelWrapper: ViewModel is null");
            }
            mViewModels.put(viewModelId, viewModel);
            return new ViewModelWrapper<T>(viewModel, true);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    /**
     * Wrapper around ViewModel instance bearing additional information
     * such as a flag indicating if the instance was created or restored
     */
    public static class ViewModelWrapper<V extends AbstractViewModel> {
        @NonNull
        private final V mViewModel;
        private final boolean mWasCreated;

        private ViewModelWrapper(@NonNull V viewModel, boolean wasCreated) {
            this.mViewModel = viewModel;
            this.mWasCreated = wasCreated;
        }


        /**
         * Provides the actual wrapped ViewModel instance
         *
         * @return ViewModel instance
         */
        @NonNull
        public V getViewModel() {
            return mViewModel;
        }


        /**
         * Returns true if the ViewModel was instantiated and not found in the static map
         *
         * @return true if the ViewModel was instantiated and not found in the static map
         */
        public boolean wasCreated() {
            return mWasCreated;
        }
    }
}
