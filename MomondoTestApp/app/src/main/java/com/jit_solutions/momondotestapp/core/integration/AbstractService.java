package com.jit_solutions.momondotestapp.core.integration;

import android.util.Log;

import com.jit_solutions.momondotestapp.core.integration.task.error.web.WebErrorUnknownException;
import com.jit_solutions.momondotestapp.core.integration.task.error.web.WebHttpErrorException;
import com.jit_solutions.momondotestapp.core.integration.task.error.web.WebTimeoutException;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_CLIENT_TIMEOUT;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static java.net.HttpURLConnection.HTTP_UNAVAILABLE;

public abstract class AbstractService {

    private static final String TAG = AbstractService.class.getName();

    public static <DTO> DTO execute(Call<DTO> call) throws Exception {
        try {
            Response<DTO> data = call.execute();

            if (data.isSuccessful()) {
                return data.body();
            } else {
                switch (data.code()) {
                    case HTTP_BAD_REQUEST:
                    case HTTP_UNAUTHORIZED:
                    case HTTP_FORBIDDEN:
                    case HTTP_UNAVAILABLE:
                    case HTTP_INTERNAL_ERROR:
                        throw new WebHttpErrorException(data.message(), data.code());
                    case HTTP_CLIENT_TIMEOUT:
                        throw new WebTimeoutException();
                    default:
                        throw new WebErrorUnknownException();
                }
            }
        } catch (SocketTimeoutException | UnknownHostException exception) {
            Log.d(TAG, "SocketOrUnknownHostException " + exception.getMessage());
            throw exception;
        } catch (Exception e) {
            Log.d(TAG, "Error executing data " + e.getMessage());
            throw e;
        }
    }
}
