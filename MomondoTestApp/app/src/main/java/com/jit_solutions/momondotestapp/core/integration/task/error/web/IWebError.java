package com.jit_solutions.momondotestapp.core.integration.task.error.web;

import com.jit_solutions.momondotestapp.core.integration.task.error.IError;

public interface IWebError extends IError {

    WebErrorCodeEnum getErrorCode();

}
