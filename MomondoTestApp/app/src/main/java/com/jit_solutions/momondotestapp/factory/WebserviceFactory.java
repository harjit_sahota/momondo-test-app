package com.jit_solutions.momondotestapp.factory;

import com.jit_solutions.momondotestapp.model.webservice.IFlightService;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WebserviceFactory {

    static IFlightService getFlightService() {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://momondo-interview.herokuapp.com/")
                .client(getDefaultHttpClient())
                .build();

        return retrofit.create(IFlightService.class);
    }

    private static OkHttpClient getDefaultHttpClient() {
        return getDefaultHttpBuilder().build();
    }

    private static OkHttpClient.Builder getDefaultHttpBuilder() {
        return new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);
    }
}
