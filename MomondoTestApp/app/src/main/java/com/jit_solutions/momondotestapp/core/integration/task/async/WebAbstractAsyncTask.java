package com.jit_solutions.momondotestapp.core.integration.task.async;


import android.util.Log;

import com.jit_solutions.momondotestapp.MomondoApplication;
import com.jit_solutions.momondotestapp.core.AbstractViewModel;
import com.jit_solutions.momondotestapp.core.integration.task.async.listener.IWebResponseListener;
import com.jit_solutions.momondotestapp.core.integration.task.error.web.IWebError;
import com.jit_solutions.momondotestapp.core.integration.task.error.web.WebErrorUnknownException;
import com.jit_solutions.momondotestapp.core.integration.task.error.web.WebHttpErrorException;
import com.jit_solutions.momondotestapp.core.integration.task.error.web.WebNetworkError;
import com.jit_solutions.momondotestapp.core.integration.task.error.web.WebTimeoutException;

import java.util.concurrent.Executor;

public abstract class WebAbstractAsyncTask<Params, Result> {

    private static final String TAG = WebAbstractAsyncTask.class.getSimpleName();

    final protected IWebResponseListener<Result> mListener;

    public WebAbstractAsyncTask(IWebResponseListener<Result> listener) {
        super();
        mListener = listener;
    }

    protected abstract Result doInBackgroundImp(Params... params) throws Exception;

    protected void onPostExecuteImp(Result result) {
    }

    protected boolean skipPreExecuteImp() {
        return false;
    }

    /**
     * Override this method and set it to true, if your task should continue,
     * even if the application is in the background. Please be aware that
     * changing the UI in the background will cause the application to crash, so
     * only set this to true, if no UI operations are being performed. (Toasts
     * are ok)
     *
     * @return
     */
    protected boolean continueInBackground() {
        return false;
    }

    public final WebAsyncTaskWrapper<Params, Result> execute(Params... params) {
        return execute(null, params);
    }

    public final WebAsyncTaskWrapper<Params, Result> execute(final Executor executor, Params... params) {

        WebAsyncTaskWrapper<Params, Result> task = new WebAsyncTaskWrapper<Params, Result>(params) {

            private IWebError networkException;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                if (skipPreExecuteImp()) {
                    return;
                }

                boolean hasInternetConnection = MomondoApplication.getSystemConfiguration().hasInternetConnection();

                if (hasInternetConnection) {
                    mListener.preExecute();
                } else {
                    cancel(true);
                    mListener.error(new WebNetworkError(), null);
                }
            }

            @Override
            protected Result doInBackground(Params... params) {

                Result result = null;

                try {
                    result = doInBackgroundImp(params);
                } catch (WebHttpErrorException httpErrorException) {
                    Log.e(TAG, "httpErrorException catched", httpErrorException);
                    networkException = httpErrorException;
                } catch (WebTimeoutException networkTimedout) {
                    Log.e(TAG, "networkTimedout catched", networkTimedout);
                    networkException = networkTimedout;
                } catch (Exception exception) {
                    Log.e(TAG, "exception catched", exception);
                    networkException = new WebErrorUnknownException();
                }
                return result;
            }

            @Override
            protected void onPostExecute(Result result) {
                super.onPostExecute(result);
                onPostExecuteImp(result);
                final AbstractViewModel listenerContext = mListener.getContext();
                if (listenerContext == null) {
                    Log.d(TAG, "Listener == null => ViewModel is null");
                    return;
                }

                try {
                    if (networkException != null) {
                        mListener.error(networkException, result);
                    } else {
                        mListener.success(result);
                    }
                } finally {
                    done();
                }
            }


            @Override
            public boolean doCancel(boolean mayInterruptIfRunning) {
                Log.d(TAG, "doCancel started");
                final boolean returnValue = cancel(mayInterruptIfRunning);
                Log.d(TAG, "doCancel executed returnValue: [" + returnValue + "]");
                try {
                    mListener.cancel();
                } catch (Exception e) {
                    Log.d(TAG, "doCancel failed" + e);
                }
                return returnValue;
            }

            @Override
            protected void onCancelled() {
                super.onCancelled();
                Log.d(TAG, "onPairingTransactionCancelled called");
                done();
            }

            private void done() {
                if (mListener != null) {
                    mListener.done();
                }
            }
        };

        WebAsyncTaskQueue.getInstance().add(task, executor);
        return task;
    }
}