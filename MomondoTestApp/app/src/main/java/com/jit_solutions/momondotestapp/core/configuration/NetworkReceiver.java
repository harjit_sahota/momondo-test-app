package com.jit_solutions.momondotestapp.core.configuration;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.jit_solutions.momondotestapp.MomondoApplication;
import com.jit_solutions.momondotestapp.core.utils.GenericListener;

public class NetworkReceiver extends BroadcastReceiver {

    interface INetworkReceiver {
        void onInternetConnection(boolean enabled);
    }

    private static boolean firstEnabledReceive, firstDisabledReceive;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (MomondoApplication.getSystemConfiguration().hasInternetConnection()) {
            if (firstEnabledReceive) return;
            firstEnabledReceive = true;
            firstDisabledReceive = false;
            GenericListener.notify(new GenericListener.Visitor<INetworkReceiver>() {

                @Override
                public void visit(INetworkReceiver listener) {

                    listener.onInternetConnection(true);
                }
            }, INetworkReceiver.class);
        } else {
            if (firstDisabledReceive) return;
            firstDisabledReceive = true;
            firstEnabledReceive = false;
            GenericListener.notify(new GenericListener.Visitor<INetworkReceiver>() {

                @Override
                public void visit(INetworkReceiver listener) {

                    listener.onInternetConnection(false);
                }
            }, INetworkReceiver.class);
        }
    }
}