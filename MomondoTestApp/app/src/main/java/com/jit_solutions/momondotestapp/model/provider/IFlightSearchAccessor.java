package com.jit_solutions.momondotestapp.model.provider;

import com.jit_solutions.momondotestapp.model.provider.model.FlightSearchModel;

import java.util.LinkedHashMap;

public interface IFlightSearchAccessor {

    FlightSearchModel getFlightSearch(LinkedHashMap<String, Object> bodyParam) throws Exception;
}
