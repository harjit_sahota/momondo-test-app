package com.jit_solutions.momondotestapp.core.integration.task.error.web;
public enum WebErrorCodeEnum {
    NO_NETWORK(-1),
    BAD_REQUEST(400),
    UNAUTHORIZED(401),
    FORBIDDEN(403),
    CLIENT_TIMEOUT(408),
    INTERNAL_ERROR(500),
    SERVICE_UNAVAILABLE(503);

    private final int statusCode;

    WebErrorCodeEnum(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }


}
