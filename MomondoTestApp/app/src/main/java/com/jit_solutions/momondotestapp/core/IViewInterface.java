package com.jit_solutions.momondotestapp.core;

import android.content.Context;
import android.databinding.ViewDataBinding;

public interface IViewInterface<Binding extends ViewDataBinding> {

    Context getContext();

    Binding getBinding();

    void enableProgressIndicator(boolean enableProgressIndicator);

    void setBackPressedEnabled(boolean enabled);

}