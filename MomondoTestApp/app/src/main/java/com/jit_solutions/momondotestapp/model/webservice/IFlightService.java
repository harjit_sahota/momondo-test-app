package com.jit_solutions.momondotestapp.model.webservice;

import com.jit_solutions.momondotestapp.model.provider.dto.FlightSearchDTO;

import java.util.LinkedHashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface IFlightService {

    @POST("/search")
    Call<FlightSearchDTO> flightSearchResults(
            @Header("Content-Type") String header,
            @Body LinkedHashMap<String, Object> body);
}
