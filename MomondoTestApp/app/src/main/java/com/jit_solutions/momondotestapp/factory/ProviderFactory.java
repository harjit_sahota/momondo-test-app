package com.jit_solutions.momondotestapp.factory;

import com.jit_solutions.momondotestapp.model.provider.FlightSearchProvider;

class ProviderFactory {

    static FlightSearchProvider getFlightSearchProvider() {
        return new FlightSearchProvider(AccessorFactory.getFlightSearchAccessor());
    }
}
