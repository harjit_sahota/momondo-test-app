package com.jit_solutions.momondotestapp.model.accessor;

import com.jit_solutions.momondotestapp.core.integration.AbstractService;
import com.jit_solutions.momondotestapp.model.provider.IFlightSearchAccessor;
import com.jit_solutions.momondotestapp.model.provider.dto.FlightSearchDTO;
import com.jit_solutions.momondotestapp.model.provider.model.FlightSearchModel;
import com.jit_solutions.momondotestapp.model.webservice.IFlightService;

import java.util.LinkedHashMap;

import retrofit2.Call;

public class FlightSearchAccessor implements IFlightSearchAccessor {
    private IFlightService mService;

    public FlightSearchAccessor(IFlightService mService) {
        this.mService = mService;
    }

    @Override
    public FlightSearchModel getFlightSearch(LinkedHashMap<String, Object> bodyParam) throws Exception {
        Call<FlightSearchDTO> call = mService.flightSearchResults("application/json", bodyParam);
        FlightSearchDTO dto = AbstractService.execute(call);
        return parseFlightDTO(dto);
    }

    private FlightSearchModel parseFlightDTO(FlightSearchDTO dto) {
        return new FlightSearchModel(
                dto.originName,
                dto.originCode,
                dto.destinationName,
                dto.destinationCode,
                dto.airlines,
                dto.airports,
                dto.ticketClasses,
                dto.flights,
                dto.legs,
                dto.segments,
                dto.suppliers,
                dto.offers);
    }
}
