package com.jit_solutions.momondotestapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.jit_solutions.momondotestapp.core.AbstractViewModelBindingActivity;
import com.jit_solutions.momondotestapp.databinding.ActivityMainBinding;
import com.jit_solutions.momondotestapp.factory.ViewModelFactory;
import com.jit_solutions.momondotestapp.flight_results.FlightFragment;
import com.jit_solutions.momondotestapp.model.provider.model.FlightSearchModel;

public class MainActivity extends AbstractViewModelBindingActivity<ActivityMainBinding, MainViewModel> implements MainViewModel.Callback {

    private static final String TAG_FLIGHT_FRAGMENT = MainActivity.class.getSimpleName() + " TAG_FLIGHT_FRAGMENT";
    private FragmentManager mFragmentManager;

    @Override
    protected MainViewModel setViewModel() {
        return ViewModelFactory.getMainViewModel();
    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_main;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getSupportFragmentManager();
    }

    public void onSearchForFlights(View view) {
        getViewModel().searchForFlights();
    }

    public void onFlightDate(View view) {
        getViewModel().getDate(view);
    }

    @Override
    public void flightSearchResults(FlightSearchModel model) {
        FlightFragment fragment = (FlightFragment) mFragmentManager.findFragmentByTag(TAG_FLIGHT_FRAGMENT);

        if (fragment == null) {
            fragment = FlightFragment.newInstance(model);
            mFragmentManager.beginTransaction()
                    .replace(R.id.flight_result_container, fragment, TAG_FLIGHT_FRAGMENT)
                    .commit();
        }
    }
}