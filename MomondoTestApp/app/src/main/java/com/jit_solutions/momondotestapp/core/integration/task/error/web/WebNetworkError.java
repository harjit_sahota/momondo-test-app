package com.jit_solutions.momondotestapp.core.integration.task.error.web;

import com.jit_solutions.momondotestapp.MomondoApplication;
import com.jit_solutions.momondotestapp.R;

public class WebNetworkError implements IWebError {

    @Override
    public String getMessage() {
        return MomondoApplication.getOkapiApplicationContext().getString(R.string.error_nonetwork_label);
    }

    @Override
    public String getTitle() {
        return MomondoApplication.getOkapiApplicationContext().getString(R.string.error_nonetwork_title);
    }

    @Override
    public WebErrorCodeEnum getErrorCode() {
        return WebErrorCodeEnum.NO_NETWORK;
    }

}
