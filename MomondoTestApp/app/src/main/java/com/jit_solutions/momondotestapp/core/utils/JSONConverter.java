package com.jit_solutions.momondotestapp.core.utils;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

public class JSONConverter {
    public static <T> String getObjectAsJSON(T object) {
        Gson gson = new GsonBuilder().create();
        JsonArray cardsAsJSON = gson.toJsonTree(object).getAsJsonArray();
        return cardsAsJSON.toString();
    }

    public static <T> String getAsJSON(T object) {
        Gson gson = new GsonBuilder().create();
        JsonObject cardsAsJSON = gson.toJsonTree(object).getAsJsonObject();
        return cardsAsJSON.toString();
    }

    public static <T> T getObjectFromJSON(String json, TypeToken typeToken) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(json, typeToken.getType());
    }


}
