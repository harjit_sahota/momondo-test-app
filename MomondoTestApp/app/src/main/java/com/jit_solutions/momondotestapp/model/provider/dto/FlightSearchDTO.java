package com.jit_solutions.momondotestapp.model.provider.dto;

import java.util.List;

public class FlightSearchDTO {
    public String originName, originCode, destinationName, destinationCode;
    public List<AirlineDTO> airlines;
    public List<AirportDTO> airports;
    public List<TicketClassDTO> ticketClasses;
    public List<FlightDTO> flights;
    public List<LegDTO> legs;
    public List<SegmentDTO> segments;
    public List<SupplierDTO>  suppliers;
    public List<OfferDTO> offers;

    public FlightSearchDTO(String originName, String originCode, String destinationName, String destinationCode, List<AirlineDTO> airlines, List<AirportDTO> airports, List<TicketClassDTO> ticketClasses, List<FlightDTO> flights, List<LegDTO> legs, List<SegmentDTO> segments, List<SupplierDTO> suppliers, List<OfferDTO> offers) {
        this.originName = originName;
        this.originCode = originCode;
        this.destinationName = destinationName;
        this.destinationCode = destinationCode;
        this.airlines = airlines;
        this.airports = airports;
        this.ticketClasses = ticketClasses;
        this.flights = flights;
        this.legs = legs;
        this.segments = segments;
        this.suppliers = suppliers;
        this.offers = offers;
    }
}
