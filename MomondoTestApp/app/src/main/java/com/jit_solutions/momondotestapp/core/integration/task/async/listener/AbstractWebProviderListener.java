package com.jit_solutions.momondotestapp.core.integration.task.async.listener;

import android.support.annotation.CallSuper;
import android.view.View;

import com.jit_solutions.momondotestapp.core.AbstractViewModel;
import com.jit_solutions.momondotestapp.core.integration.task.error.web.IWebError;
import com.jit_solutions.momondotestapp.core.integration.task.error.web.WebErrorCodeEnum;

public abstract class AbstractWebProviderListener<T> implements IWebResponseListener<T> {

    private IContextProviderListener mContextProviderListener;
    private boolean mCancelled = false;
    private View mRetryViewContainer;
    private View.OnClickListener mRetryHandler;

    public AbstractWebProviderListener(IContextProviderListener contextProviderListener) {
        mContextProviderListener = contextProviderListener;
    }

    public AbstractWebProviderListener(IContextProviderListener contextProviderListener, View retryViewContainer, View.OnClickListener retryHandler) {
        mRetryViewContainer = retryViewContainer;
        mRetryHandler = retryHandler;
        mContextProviderListener = contextProviderListener;
    }

    @Override
    public void preExecute() {
    }

    @CallSuper
    @Override
    public void success(T result) {
    }

    @Override
    public void error(IWebError networkError, T result) {
        if (networkError.getErrorCode() == WebErrorCodeEnum.NO_NETWORK) {
            setErrorView();
        }
    }

    @Override
    public void done() {

    }

    @Override
    public void cancel() {
        mCancelled = true;
    }


    @Override
    public AbstractViewModel getContext() {
        return mContextProviderListener.getContextProviderForAsyncTask();
    }

    protected boolean isCancelled() {
        return mCancelled;
    }

    private void setErrorView() {
    }

}
