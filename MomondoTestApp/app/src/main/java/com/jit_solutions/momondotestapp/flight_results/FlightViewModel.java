package com.jit_solutions.momondotestapp.flight_results;

import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.jit_solutions.momondotestapp.BR;
import com.jit_solutions.momondotestapp.R;
import com.jit_solutions.momondotestapp.core.AbstractViewModel;
import com.jit_solutions.momondotestapp.databinding.FlightFragmentBinding;
import com.jit_solutions.momondotestapp.databinding.ItemFlightsBinding;
import com.jit_solutions.momondotestapp.model.provider.dto.AirlineDTO;
import com.jit_solutions.momondotestapp.model.provider.dto.AirportDTO;
import com.jit_solutions.momondotestapp.model.provider.dto.LegDTO;
import com.jit_solutions.momondotestapp.model.provider.model.FlightResultModel;
import com.jit_solutions.momondotestapp.model.provider.model.FlightSearchModel;

import java.util.ArrayList;
import java.util.List;

public class FlightViewModel extends AbstractViewModel<FlightFragmentBinding> {
    private ArrayList<FlightResultModel> mFlightResultModelList;
    private FlightResultAdapter mAdapter;

    @Override
    protected void addViewModelObservableBinding(FlightFragmentBinding binding) {
        binding.setViewmodel(this);
    }

    void getFlightSearchModel(FlightSearchModel model) {
        if (mAdapter == null)
            mAdapter = new FlightResultAdapter();

        mFlightResultModelList = new ArrayList<>();
        notifyPropertyChanged(BR.data);

        List<LegDTO> legs = model.getLegs();
        for (int i = 0; i < legs.size(); i++) {
            FlightResultModel flightResultModel = new FlightResultModel();

            int airlineIndex = legs.get(i).getAirlineIndex();
            List<AirlineDTO> airlines = model.getAirlines();

            for (int j = 0; j < airlines.size(); j++) {
                if (airlineIndex == airlines.get(j).id) {

                    flightResultModel.setAirline(airlines.get(j).name);
                    break;
                }
            }

            int destinationIndex = legs.get(i).getDestinationIndex();
            int originIndex = legs.get(i).getOriginIndex();
            List<AirportDTO> airports = model.getAirports();

            for (int k = 0; k < airports.size(); k++) {
                if (destinationIndex == airports.get(k).id) {
                    flightResultModel.setDestination(airports.get(k).name);
                }

                if (originIndex == airports.get(k).id) {
                    flightResultModel.setOrigin(airports.get(k).name);
                }
            }

            flightResultModel.setFlightNumber(legs.get(i).getFlightNumber());
            flightResultModel.setDeparture(legs.get(i).getDeparture());
            flightResultModel.setArrival(legs.get(i).getArrival());

            mFlightResultModelList.add(flightResultModel);
        }
    }

    @BindingAdapter(value = {"setSrc", "setAdapter"})
    public static void setSrc(RecyclerView recyclerView, ArrayList<FlightResultModel> data, FlightResultAdapter adapter) {
        if (adapter != null) {
            adapter.setData(data);
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(adapter);
        }
    }

    @Bindable
    public FlightResultAdapter getAdapter() {
        return mAdapter;
    }

    @Bindable
    public ArrayList<FlightResultModel> getData() {
        return mFlightResultModelList;
    }

    public class FlightResultAdapter extends RecyclerView.Adapter<FlightResultAdapter.FlightViewHolder> {
        ArrayList<FlightResultModel> mFlightResultModels;

        @Override
        public FlightViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            ItemFlightsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_flights, parent, false);
            return new FlightViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(FlightViewHolder vh, int position) {
            vh.bind(new FlightItemViewModel(mFlightResultModels.get(position)));
        }

        @Override
        public int getItemCount() {
            return mFlightResultModels.size();
        }

        public void setData(ArrayList<FlightResultModel> data) {
            mFlightResultModels = data;
            notifyDataSetChanged();
        }

        class FlightViewHolder extends RecyclerView.ViewHolder {
            ItemFlightsBinding itemFlightsBinding;

            FlightViewHolder(ItemFlightsBinding binding) {
                super(binding.getRoot());
                this.itemFlightsBinding = binding;
            }

            void bind(FlightItemViewModel item) {
                itemFlightsBinding.setObservable(item);
                itemFlightsBinding.executePendingBindings();
            }
        }
    }
}